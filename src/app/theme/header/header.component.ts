import { Component, Input,Output, OnInit, ChangeDetectorRef, AfterViewInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit,AfterViewInit{

  @Input() isCourseVisible: Boolean = true;
  constructor(public cd: ChangeDetectorRef) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    alert(this.isCourseVisible);
    this.cd.detectChanges();
  }

}
